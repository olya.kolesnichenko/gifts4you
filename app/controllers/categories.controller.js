/**
 * Created by Sunny on 24.01.17.
 */
angular.module('gifts4you')
    .controller('CategoriesController',
    function ($scope, $http) {

        $http.get('/data/categories.json').success(function (data) {
            $scope.categories = data;
        });

    });
