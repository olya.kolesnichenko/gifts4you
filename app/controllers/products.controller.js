/**
 * Created by Sunny on 07.09.16.
 */

angular.module('gifts4you')
    .controller('ProductsController',
    function ($scope, $http) {

        /*$http({
            method: 'GET',
            url: SERVER_URL+'products'
        }).then(function successCallback(response) {
            $scope.products = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });*/
        $http.get('data/products.json').success(function (data) {
            $scope.products = data;
        });

    });
