/**
 * Created by Sunny on 02.11.16.
 */
angular.module('gifts4you')
    .controller('ProductController',
    function ($scope, $http) {

        $http({
            method: 'GET',
            params: {
                product_id: 1
            },
            url: SERVER_URL+'product'
        }).then(function successCallback(response) {
            $scope.product = response.data.product;
            $scope.shop = response.data.shop;
            $scope.deliveryTypes = response.data.deliveryTypes;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    });
