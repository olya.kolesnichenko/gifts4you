/**
 * Created by ovi on 27/08/16.
 */


angular.module('gifts4you')
    .controller('RegistrationController',
        function ($scope, $http) {


            $scope.register = function(user) {
                $http({
                    method: 'GET',
                    params: user,
                    url: SERVER_URL+'registration'
                }).then(function successCallback(response) {
                    $scope.message = response.data;
                }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            };

        });


