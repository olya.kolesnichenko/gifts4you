/**
 * Created by ovi on 27/08/16.
 */


angular.module('gifts4you')
    .controller('LoginController',
        function ($scope, $http) {


            $scope.login = function(user) {
                $http({
                    method: 'GET',
                    params: user,
                    url: SERVER_URL+'login'
                }).then(function successCallback(response) {
                    $scope.message = response.data;
                }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            };

        });


