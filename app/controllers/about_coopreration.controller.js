(function () {

    'use strict';

    var app = angular
        .module('gifts4you')
        .controller('AboutCooperationController', ['$scope', 'AboutService', AboutCooperationController]);

    function AboutCooperationController($scope, AboutService) {


        AboutService.getAboutCooperation().then(function (data) {

            $scope.about = data;
        });

    }
})();