/**
 * Created by ovi on 27/08/16.
 */


(function () {

    'use strict';
    var MAX_LEN = 200;
    var WARN_LEN = 10;
    var app = angular
        .module('gifts4you')
        .controller('FeedbackClientController', ['$scope', 'FeedbackService', FeedbackClientController]);

    function FeedbackClientController($scope, FeedbackService) {

        /* $scope.feedback = {};
         $scope.submit = function () {

         FeedbackService.postFeedbackClient($scope.feedback).then(function (data) {
         if (data) {
         //alert on successed
         alert("success");
         $scope.feedback = {};
         }else{
         //alert on error
         alert("fail");
         }
         });
         */


        $scope.user = {
            text:""
        };

        $scope.remaining = function () {
           // console.log($scope.feedback_client.text);
            return MAX_LEN - $scope.feedback_client.text.$viewValue.length;
        };
        $scope.shouldWarn = function () {
            console.log('warn');
            console.log($scope.remaining());
            return WARN_LEN > $scope.remaining();
        };


        $scope.submitForm = function () {

            // check to make sure the form is completely valid
            if ($scope.feedback_client.$valid) {
                alert('form has been sent');
            }

        }


    }
})();