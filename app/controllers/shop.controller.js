/**
 * Created by Sunny on 07.11.16.
 */

angular.module('gifts4you')
    .controller('ShopController',
    function ($scope, $http) {

        $http({
            method: 'GET',
            params: {
                shop_id: 1
            },
            url: SERVER_URL+'shop'
        }).then(function successCallback(response) {
            $scope.shop = response.data.shop;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    });
