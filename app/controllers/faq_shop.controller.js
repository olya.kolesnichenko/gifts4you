
(function () {

    'use strict';

    var app = angular
        .module('gifts4you')
        .controller('FaqShopController', ['$scope', 'FaqService', FaqShopController]);

    function FaqShopController($scope, FaqService) {

        $scope.url = document.location.href;
        FaqService.getFaqShop().then(function (data) {
            $scope.faqs = data;
        });
    }
})();