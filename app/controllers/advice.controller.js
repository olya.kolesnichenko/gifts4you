/**
 * Created by ovi on 27/08/16.
 */


(function () {

    'use strict';

    var app = angular
        .module('gifts4you')
        .controller('AdviceController', ['$scope', 'AdviceService', AdviceController]);

    function AdviceController($scope, AdviceService) {

        $scope.url = document.location.href;
        AdviceService.getAdvice().then(function (data) {

            $scope.advice = data;
        });

    }
})();