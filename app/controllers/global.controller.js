/**
 * Created by ovi on 27/08/16.
 */


angular.module('gifts4you')
    .controller('GlobalController',
    function ($scope, $http) {

        $scope.authorized = false;
        setTimeout(function(){

            $scope.authorized = true;
            $scope.$apply();

        }, 3000);
    });
