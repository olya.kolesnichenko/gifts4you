/**
 * Created by Sunny on 27.01.17.
 */
(function () {
    'use strict';
    angular.module('gifts4you')
        .factory('AdviceService', ['$http', AdviceService]);

    function AdviceService($http, $window) {

        return {
            getAdvice: getAdvice


        };

        function getAdvice() {
            return  $http.get('data/advice.json').then(function(res){
                return res.data;
            });
        }



    }
})();